#De la imagen que partimos
FROM adoptopenjdk/openjdk8:latest

#Directorio de trabajo
WORKDIR /app

#Copiamos el uber-jar en el directorio de trabajo
COPY target/desafio-0.0.1.jar /app

#Exponemos el puerto 8082
EXPOSE 8082

#Comando que se ejecutará una vez ejecutemos el contendor
CMD ["java","-jar","desafio-0.0.1.jar"]