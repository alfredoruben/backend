package com.bcp.desafio.view;

import java.io.Serializable;

public class UpdateTipoCambioRequest implements Serializable{
/**
	 * 
	 */
private static final long serialVersionUID = 1L;

private String codigoMoneda;
private Double tipoCambioSol;

	public String getCodigoMoneda() {
		return codigoMoneda;
	}

	public void setCodigoMoneda(String codigoMoneda) {
		this.codigoMoneda = codigoMoneda;
	}

	public Double getTipoCambioSol() {
		return tipoCambioSol;
	}

	public void setTipoCambioSol(Double tipoCambioSol) {
		this.tipoCambioSol = tipoCambioSol;
	}
}
