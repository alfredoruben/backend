package com.bcp.desafio.service;

import com.bcp.desafio.entity.TipoCambio;
import com.bcp.desafio.view.TipoCambioRequest;

import com.bcp.desafio.view.TipoCambioResponse;
import com.bcp.desafio.view.UpdateTipoCambioRequest;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;

public interface TipoCambioService {
	Single<TipoCambioResponse> convertir(TipoCambioRequest tipoCambioRequest);
	public void guardar(TipoCambio tipoCambio);

    Completable actualizarTipoCambio(UpdateTipoCambioRequest updateTipoCambioRequest);

	public Single<List<TipoCambio>> listar();

	Completable insertaTipoCambios(List<TipoCambio> listaTipoCambio);
}
