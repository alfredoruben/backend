package com.bcp.desafio.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.bcp.desafio.view.TipoCambioResponse;
import com.bcp.desafio.view.UpdateTipoCambioRequest;
import io.reactivex.Completable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.desafio.entity.TipoCambio;
import com.bcp.desafio.repository.TipoCambioRepository;
import com.bcp.desafio.view.TipoCambioRequest;

import io.reactivex.Single;

import javax.persistence.EntityNotFoundException;

@Service
public class TipoCambioServiceImpl implements TipoCambioService{

	@Autowired
	private TipoCambioRepository tipoCambioRepository;
	
	@Override
	public Single<TipoCambioResponse> convertir(TipoCambioRequest tipoCambioRequest) {
		 return addCambioToRepository(tipoCambioRequest);
	}
    private Single<TipoCambioResponse> addCambioToRepository(TipoCambioRequest tipoCambioRequest) {
        return Single.create(singleSubscriber -> {
            
            TipoCambioResponse cambioResponse=new TipoCambioResponse(); 
            
            TipoCambio tcOrigen=tipoCambioRepository.findByCodigo(tipoCambioRequest.getMonedaOrigen());
            TipoCambio tcFinal=tipoCambioRepository.findByCodigo(tipoCambioRequest.getMonedaDestino());
            double montoSoles=0;
            double montoFinal=0;
            
            if(!tcOrigen.getCodigo().equals("PEN")) {
            	//convierto a soles
            	montoSoles=tipoCambioRequest.getMonto()*tcOrigen.getTipoCambioSol();
            	//convierto a moneda final
            	montoFinal=montoSoles/tcFinal.getTipoCambioSol();            	
            }else {
            	// esta en soles
            	montoFinal=tipoCambioRequest.getMonto()/tcFinal.getTipoCambioSol();
            }
            BigDecimal bdMontoFinal = BigDecimal.valueOf(montoFinal);
            bdMontoFinal = bdMontoFinal.setScale(3, RoundingMode.HALF_UP);
            cambioResponse.setMonto(tipoCambioRequest.getMonto());  
            cambioResponse.setMontoTipoCambio(bdMontoFinal.doubleValue());
            cambioResponse.setMonedaOrigen(tcOrigen.getDescripcion());
            cambioResponse.setMonedaDestino(tcFinal.getDescripcion());
            BigDecimal bdTc = BigDecimal.valueOf(cambioResponse.getMontoTipoCambio()/tipoCambioRequest.getMonto());
            bdTc = bdTc.setScale(3, RoundingMode.HALF_UP);
            cambioResponse.setTipoCambio(bdTc.doubleValue() + tcFinal.getCodigo());            
            singleSubscriber.onSuccess(cambioResponse);
        });
    }
	@Override
	public void guardar(TipoCambio tipoCambio) {
		tipoCambioRepository.save(tipoCambio);		
	}

    @Override
    public Completable actualizarTipoCambio(UpdateTipoCambioRequest updateTipoCambioRequest) {

	    return updateTipoCambioToRepository(updateTipoCambioRequest);
    }

    @Override
    public Single<List<TipoCambio>> listar() {
        return Single.create(singleSubscriber -> {
            singleSubscriber.onSuccess(tipoCambioRepository.findAll());
        });
    }

    @Override
    public Completable insertaTipoCambios(List<TipoCambio> listaTipoCambio) {
        return Completable.create(completableSubscriber -> {
            try{
                tipoCambioRepository.saveAll(listaTipoCambio);

                completableSubscriber.onComplete();
            }catch (Exception e){
                completableSubscriber.onError(new Exception());
            }
        });
    }


    private Completable updateTipoCambioToRepository(UpdateTipoCambioRequest updateTipoCambioRequest) {
        return Completable.create(completableSubscriber -> {
            TipoCambio tipoCambio = tipoCambioRepository.findByCodigo(updateTipoCambioRequest.getCodigoMoneda());
            if (tipoCambio==null)
                completableSubscriber.onError(new EntityNotFoundException());
            else {
                tipoCambio.setTipoCambioSol(updateTipoCambioRequest.getTipoCambioSol());
                tipoCambioRepository.save(tipoCambio);
                completableSubscriber.onComplete();
            }
        });
    }

}
