package com.bcp.desafio.web;

import com.bcp.desafio.entity.TipoCambio;
import com.bcp.desafio.view.TipoCambioResponse;
import com.bcp.desafio.view.UpdateTipoCambioRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.bcp.desafio.response.BaseWebResponse;
import com.bcp.desafio.service.TipoCambioService;
import com.bcp.desafio.view.TipoCambioRequest;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

@RestController
@RequestMapping(value = "/api/cambio")
public class TipoCambioController {
	
    @Autowired
    private TipoCambioService tipoCambioService;
    
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<TipoCambioResponse>>> getConversion(@RequestBody TipoCambioRequest addAuthorWebRequest) {
       
    	return tipoCambioService.convertir(addAuthorWebRequest)
                .subscribeOn(Schedulers.io())
                .map(tipoCambioResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(tipoCambioResponse)));
    	    	
    }



    @PutMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse>> updateTipoCambio(@RequestBody UpdateTipoCambioRequest updateTipoCambioRequest) {
        return tipoCambioService.actualizarTipoCambio(updateTipoCambioRequest)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
    }

    @GetMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse<List<TipoCambio>>>> getConversion() {

        return tipoCambioService.listar()
                .subscribeOn(Schedulers.io())
                .map(tipoCambioResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(tipoCambioResponse)));

    }


    @PostMapping(path="/inserta",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Single<ResponseEntity<BaseWebResponse>> insertaTC(@RequestBody List<TipoCambio> listaTipoCambio) {
        return tipoCambioService.insertaTipoCambios(listaTipoCambio)
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(BaseWebResponse.successNoData()));
    }
    
}
