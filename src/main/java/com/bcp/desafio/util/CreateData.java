package com.bcp.desafio.util;

import com.bcp.desafio.security.entity.Usuario;
import com.bcp.desafio.security.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.bcp.desafio.entity.TipoCambio;
import com.bcp.desafio.security.entity.Rol;
import com.bcp.desafio.security.enums.RolNombre;
import com.bcp.desafio.security.service.RolService;
import com.bcp.desafio.service.TipoCambioService;

import java.util.HashSet;
import java.util.Set;


/**
*crea los roles al ejecutar la aplicacion
 *
 */

@Component
public class CreateData implements CommandLineRunner {

    @Autowired
    RolService rolService;
    @Autowired
    TipoCambioService tipoCambioService;
    @Autowired
    UsuarioService usuarioService;

    @Override
    public void run(String... args) throws Exception {
        Rol rolAdmin = new Rol(RolNombre.ROLE_ADMIN);
        Rol rolUser = new Rol(RolNombre.ROLE_USER);
        rolService.save(rolAdmin);
        rolService.save(rolUser);
        
        
        TipoCambio tc=new TipoCambio();
        tc.setCodigo("USD");
        tc.setDescripcion("Dólar estadounidense");
        tc.setTipoCambioSol(3.729);
        tipoCambioService.guardar(tc);
        
        tc=new TipoCambio();
        tc.setCodigo("EUR");
        tc.setDescripcion("Euro");
        tc.setTipoCambioSol(4.353);
        tipoCambioService.guardar(tc);
        
        tc=new TipoCambio();
        tc.setCodigo("JPY");
        tc.setDescripcion("Yen Japonés");
        tc.setTipoCambioSol(0.033);
        tipoCambioService.guardar(tc);
        
        tc=new TipoCambio();
        tc.setCodigo("MXN");
        tc.setDescripcion("Peso Mexicano");
        tc.setTipoCambioSol(0.180);
        tipoCambioService.guardar(tc);
        
        tc=new TipoCambio();
        tc.setCodigo("PEN");
        tc.setDescripcion("Soles");
        tc.setTipoCambioSol(1.0);
        tipoCambioService.guardar(tc);

        //admin
        //$2a$10$j7R79ziuc4tFhBILcVY.B.XF3tKFweQV208N8XKv5zKeKuhDDeIYy
        Usuario usuario=new Usuario("admin","admin","alfredfis@gmail.com","$2a$10$j7R79ziuc4tFhBILcVY.B.XF3tKFweQV208N8XKv5zKeKuhDDeIYy");
        Set<Rol> roles = new HashSet<>();
        roles.add(rolAdmin);
        usuario.setRoles(roles);
        usuarioService.save(usuario);




        /*
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(1,'Dólar estadounidense','USD', 3.729);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(2,'Euro','EUR', 4.353);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(3,'Yen Japonés','JPY', 0.033);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(4,'Soles','PEN', 1);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(5,'Peso Mexicano','MXN', 0.180);
      */
    }
}
